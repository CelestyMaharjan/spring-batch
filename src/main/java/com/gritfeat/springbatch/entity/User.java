package com.gritfeat.springbatch.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Table(name = "batch_user_tbl")
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    private String name;
    private String address;
    private int contact;

    public User (String name, String address, int contact) {
        this.name = name;
        this.address = address;
        this.contact = contact;
    }
}
