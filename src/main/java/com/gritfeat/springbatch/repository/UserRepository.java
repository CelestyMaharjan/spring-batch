package com.gritfeat.springbatch.repository;

import com.gritfeat.springbatch.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
