package com.gritfeat.springbatch.batches;

import com.gritfeat.springbatch.entity.User;
import com.gritfeat.springbatch.repository.UserRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class UserItemWriter implements ItemWriter<User> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void write (List<? extends User> users) throws Exception {
        System.out.println("writer");
        userRepository.saveAll(users);
    }
}
