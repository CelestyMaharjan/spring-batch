package com.gritfeat.springbatch.batches;

import com.gritfeat.springbatch.entity.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserItemProcessor implements ItemProcessor<User, User> {

    @Override
    public User process (User user) throws Exception {
        String userName = user.getName().toUpperCase();
        String userAddress = user.getAddress().toLowerCase();
        int userContact = user.getContact();
        User userModified = new User(userName, userAddress, userContact);

        System.out.println("processor");
        return userModified;
    }
}
