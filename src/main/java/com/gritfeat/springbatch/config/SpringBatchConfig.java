package com.gritfeat.springbatch.config;

import com.gritfeat.springbatch.batches.UserItemProcessor;
import com.gritfeat.springbatch.entity.User;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
//import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.*;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;


@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;
    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Value("${file.input}")
    private String fileInput;

//    public Job job (Step step) {
//
//    }


//
//    public Step step (JdbcBatchItemWriter writer) {
//        return stepBuilderFactory.get("step-builder")
//                .<User, User>chunk(10)
//                .reader(fileItemReader())
//                .processor(processor())
//                .writer(writer)
//                .build();
//    }
//
//    public Job job (Step step) {
//        return jobBuilderFactory.get("job builder")
//                .incrementer(new RunIdIncrementer())
//                .start(step)
//                .build();
//    }

    @Bean
    public Job job (JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
                    ItemReader<User> itemReader,
                    ItemProcessor<User, User> itemProcessor,
                    ItemWriter<User> itemWriter) {

        Step step = stepBuilderFactory.get("step-builder")
                .<User, User>chunk(10)
                .reader(itemReader())
                .processor(itemProcessor())
                .writer(itemWriter)
                .build();

        return jobBuilderFactory.get("job-builder")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();

    }


    //item reader
    @Bean
    public FlatFileItemReader<User> itemReader () {
        FlatFileItemReader<User> userReader = new FlatFileItemReader<>();
        userReader.setName("item-reader");
        userReader.setResource(new ClassPathResource(fileInput));
        userReader.setLinesToSkip(1);
        userReader.setLineMapper(new DefaultLineMapper() {
            {
                setLineTokenizer(new DelimitedLineTokenizer() {
                    {
                        setNames(new String[]{"name", "address", "contact"});
                        setDelimiter(",");
                    }
                });

                setFieldSetMapper(new BeanWrapperFieldSetMapper<User>() {
                    {
                        setTargetType(User.class);
                    }
                });
            }
        });

        return userReader;

//        return new FlatFileItemReaderBuilder<User>()
//                .name("item-reader")
//                .resource(new ClassPathResource(fileInput))
////                .resource(resource)
//                .linesToSkip(1)
//                .delimited()
//                .names(new String[]{"name", "address", "contact"})
//                .fieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
//                    setTargetType(User.class);
//                }})
//                .build();
    }

    //item processor
    public UserItemProcessor itemProcessor () {
        return new UserItemProcessor();
    }
//
//    //item writer
//    public JdbcBatchItemWriter itemWriter (DataSource dataSource) {
//        return new JdbcBatchItemWriterBuilder<User>()
//                .itemSqlParameterSourceProvider(
//                        new BeanPropertyItemSqlParameterSourceProvider<>()
//                ).sql("INSERT INTO user_tbl (name, address, contact) VALUES (:name, :address, :contact)")
//                .dataSource(dataSource)
//                .build();
//    }

}
